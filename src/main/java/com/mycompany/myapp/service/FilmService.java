package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.FilmDTO;

public interface FilmService
{
	long count();
	FilmDTO findOne(Short id);
	List<FilmDTO> findAll();
}
