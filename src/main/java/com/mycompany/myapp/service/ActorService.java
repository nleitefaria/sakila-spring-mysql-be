package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.ActorDTO;

public interface ActorService
{
	long count();
	ActorDTO findOne(Short id);
	List<ActorDTO> findAll();
	void save(ActorDTO actorDTO);
	ActorDTO update(short id, ActorDTO actorDTO) throws javax.persistence.RollbackException;
	Short delete(short id) throws javax.persistence.RollbackException;
	void deleteAll();
}
