package com.mycompany.myapp.service;

import java.util.List;
import com.mycompany.myapp.domain.RentalDTO;

public interface RentalService
{
	long count(); 
	RentalDTO findOne(Integer id);
	List<RentalDTO> findAll();
}
