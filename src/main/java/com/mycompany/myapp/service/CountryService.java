package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.CountryDTO;

public interface CountryService 
{
	CountryDTO findOne(Short id);
	List<CountryDTO> findAll();
	void save(CountryDTO country);
	CountryDTO update(short id, CountryDTO countryDTO) throws javax.persistence.RollbackException;
	Short delete(short id) throws javax.persistence.RollbackException;
	void deleteAll();
}
