package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.CustomerDTO;

public interface CustomerService 
{
	long count();
	CustomerDTO findOne(Short id);
	List<CustomerDTO> findAll();
	void save(CustomerDTO customerDTO);

}
