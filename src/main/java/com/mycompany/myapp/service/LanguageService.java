package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.LanguageDTO;

public interface LanguageService
{
	long count();
	LanguageDTO findOne(Short id);
	List<LanguageDTO> findAll();
	void save(LanguageDTO languageDTO);
	LanguageDTO update(short id, LanguageDTO languageDTO) throws javax.persistence.RollbackException;
	Short delete(short id) throws javax.persistence.RollbackException;
	void deleteAll();
}
