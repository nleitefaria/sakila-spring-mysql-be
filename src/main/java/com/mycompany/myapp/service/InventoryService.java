package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.InventoryDTO;

public interface InventoryService
{
	long count();
	InventoryDTO findOne(Integer id);
	List<InventoryDTO> findAll();
}
