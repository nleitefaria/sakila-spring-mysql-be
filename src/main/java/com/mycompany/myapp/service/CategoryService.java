package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.CategoryDTO;

public interface CategoryService
{
	long count();
	CategoryDTO findOne(Short id);
	List<CategoryDTO> findAll();
	void save(CategoryDTO categoryDTO);
	public CategoryDTO update(short id, CategoryDTO categoryDTO) throws javax.persistence.RollbackException;
	Short delete(short id) throws javax.persistence.RollbackException;
	void deleteAll();
}
