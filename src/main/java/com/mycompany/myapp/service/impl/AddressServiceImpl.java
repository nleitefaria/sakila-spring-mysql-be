package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.AddressDTO;
import com.mycompany.myapp.domain.CityDTO;
import com.mycompany.myapp.domain.CountryDTO;
import com.mycompany.myapp.entity.Address;
import com.mycompany.myapp.entity.City;
import com.mycompany.myapp.entity.Country;
import com.mycompany.myapp.repository.AddressRepository;
import com.mycompany.myapp.repository.CityRepository;
import com.mycompany.myapp.repository.CountryRepository;
import com.mycompany.myapp.service.AddressService;

@Service
public class AddressServiceImpl implements AddressService 
{	
	@Autowired
	AddressRepository addressRepository;
	
	@Autowired
	CityRepository cityRepository;
	
	@Autowired
	CountryRepository countryRepository;
	
	@Transactional
	public long count() 
	{		
		return addressRepository.count();		
	}
	
	@Transactional
	public AddressDTO findOne(Short id) 
	{
		Address address = addressRepository.findOne(id);		
		CityDTO city = new CityDTO(address.getCity().getCityId(), new CountryDTO(address.getCity().getCountry().getCountryId(), address.getCity().getCountry().getCountry(), address.getCity().getCountry().getLastUpdate()), address.getCity().getCity(), address.getCity().getLastUpdate());
		return new AddressDTO(address.getAddressId(), city, address.getAddress(), address.getAddress2(), address.getDistrict(), address.getPostalCode(), address.getPhone(), address.getLastUpdate());
	}
	
	@Transactional
	public List<AddressDTO> findAll() 
	{	
		List<AddressDTO > ret = new ArrayList<AddressDTO>();		
		for(Address address : addressRepository.findAll())
		{
			CityDTO city = new CityDTO(address.getCity().getCityId(), new CountryDTO(address.getCity().getCountry().getCountryId(), address.getCity().getCountry().getCountry(), address.getCity().getCountry().getLastUpdate()), address.getCity().getCity(), address.getCity().getLastUpdate());
			ret.add(new AddressDTO(address.getAddressId(), city, address.getAddress(), address.getAddress2(), address.getDistrict(), address.getPostalCode(), address.getPhone(), address.getLastUpdate()));			
		}
		return ret;
	}
		
	@Transactional
	public void save(AddressDTO addressDTO) 
	{
		City city = cityRepository.findOne(addressDTO.getCity().getCityId());		
		if(city == null)
		{
			Country country = countryRepository.findOne(addressDTO.getCity().getCountry().getCountryId());
			
			if(country == null)
			{
				country = new Country(addressDTO.getCity().getCountry().getCountry(), new Date());
				countryRepository.save(country);
			}
			
			city = new City(country, addressDTO.getCity().getCity(), new Date());
			cityRepository.save(city);			
		}		
		Address address = new Address(city, addressDTO.getAddress(), addressDTO.getDistrict(), addressDTO.getPhone(), addressDTO.getLastUpdate());
		addressRepository.save(address);		
	}

}
