package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.AddressDTO;
import com.mycompany.myapp.domain.CityDTO;
import com.mycompany.myapp.domain.CountryDTO;
import com.mycompany.myapp.domain.StaffDTO;
import com.mycompany.myapp.domain.StoreDTO;
import com.mycompany.myapp.entity.Address;
import com.mycompany.myapp.entity.Staff;
import com.mycompany.myapp.entity.Store;
import com.mycompany.myapp.repository.AddressRepository;
import com.mycompany.myapp.repository.StaffRepository;
import com.mycompany.myapp.repository.StoreRepository;
import com.mycompany.myapp.service.StoreService;

@Service
public class StoreServiceImpl implements StoreService
{
	@Autowired
	StoreRepository storeRepository;
	
	@Autowired
	AddressRepository addressRepository;
	
	@Autowired
	StaffRepository staffRepository;

	@Transactional
	public long count() 
	{		
		return storeRepository.count();		
	}
	
	@Transactional
	public StoreDTO findOne(Short id) 
	{
		Store store = storeRepository.findOne(id);		
		Address address = store.getAddress();
		Staff staff = store.getStaff();		
		CityDTO city = new CityDTO(address.getCity().getCityId(), new CountryDTO(address.getCity().getCountry().getCountryId(), address.getCity().getCountry().getCountry(), address.getCity().getCountry().getLastUpdate()), address.getCity().getCity(), address.getCity().getLastUpdate());
		return new StoreDTO(store.getStoreId() , new AddressDTO(address.getAddressId(), city, address.getAddress(), address.getAddress2(), address.getDistrict(), address.getPostalCode(), address.getPhone(), address.getLastUpdate()), new StaffDTO(staff.getStaffId(), staff.getFirstName(),staff.getLastName(), staff.getPicture(), staff.getEmail(), staff.isActive(), staff.getUsername(), staff.getPassword(), staff.getLastUpdate()),store.getLastUpdate());
	}
	
	@Transactional
	public List<StoreDTO> findAll() 
	{	
		List<StoreDTO> ret = new ArrayList<StoreDTO>();		
		for(Store store : storeRepository.findAll())
		{
			Address address = store.getAddress();	
			Staff staff = store.getStaff();		
			CityDTO city = new CityDTO(address.getCity().getCityId(), new CountryDTO(address.getCity().getCountry().getCountryId(), address.getCity().getCountry().getCountry(), address.getCity().getCountry().getLastUpdate()), address.getCity().getCity(), address.getCity().getLastUpdate());
			ret.add(new StoreDTO(store.getStoreId() , new AddressDTO(address.getAddressId(), city, address.getAddress(), address.getAddress2(), address.getDistrict(), address.getPostalCode(), address.getPhone(), address.getLastUpdate()), new StaffDTO(staff.getStaffId(), staff.getFirstName(),staff.getLastName(), staff.getPicture(), staff.getEmail(), staff.isActive(), staff.getUsername(), staff.getPassword(), staff.getLastUpdate()),store.getLastUpdate()));		
		}
		return ret;
	}
	
	@Transactional
	public void save(StoreDTO storeDTO) 
	{		
		Address address = addressRepository.findOne(storeDTO.getAddress().getAddressId());
		Staff staff = staffRepository.findOne(storeDTO.getStaff().getStaffId());
		Store store = new Store(address, staff, new Date());
		storeRepository.save(store);		
	}
	
	
	@Transactional
	public StoreDTO update(short id, StoreDTO storeDTO) throws javax.persistence.RollbackException   
	{		
		Store store = storeRepository.findOne(id);
		
		if(store != null)
		{			
			Address address = addressRepository.findOne(storeDTO.getAddress().getAddressId());
						
			if(address != null)
			{
				store.setAddress(address);					
				CityDTO city = new CityDTO(address.getCity().getCityId(), new CountryDTO(address.getCity().getCountry().getCountryId(), address.getCity().getCountry().getCountry(), address.getCity().getCountry().getLastUpdate()), address.getCity().getCity(), address.getCity().getLastUpdate());
				//AddressDTO addressDTO  = new AddressDTO(address.getAddressId(), city, address.getAddress(), address.getAddress2(), address.getDistrict(), address.getPostalCode(), address.getPhone(), address.getLastUpdate());			
				Staff staff = staffRepository.findOne(storeDTO.getStaff().getStaffId());			
				StoreDTO ret = new StoreDTO(store.getStoreId() , new AddressDTO(address.getAddressId(), city, address.getAddress(), address.getAddress2(), address.getDistrict(), address.getPostalCode(), address.getPhone(), address.getLastUpdate()), new StaffDTO(staff.getStaffId(), staff.getFirstName(),staff.getLastName(), staff.getPicture(), staff.getEmail(), staff.isActive(), staff.getUsername(), staff.getPassword(), staff.getLastUpdate()),store.getLastUpdate());
				return ret;												
			}
			else
			{
				return null;
			}					
		}
		else
		{
			return null;		
		}			
	}
	
	

}
