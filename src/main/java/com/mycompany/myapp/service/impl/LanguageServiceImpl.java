package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.LanguageDTO;
import com.mycompany.myapp.entity.Language;
import com.mycompany.myapp.repository.LanguageRepository;
import com.mycompany.myapp.service.LanguageService;

@Service
public class LanguageServiceImpl implements LanguageService
{
	@Autowired
	LanguageRepository languageRepository;

	@Transactional
	public long count() 
	{		
		return languageRepository.count();		
	}
	
	@Transactional
	public LanguageDTO findOne(Short id) 
	{
		Language language = languageRepository.findOne(id);	
		return new LanguageDTO(language.getLanguageId(), language.getName(), language.getLastUpdate());
	}
	
	@Transactional
	public List<LanguageDTO> findAll() 
	{	
		List<LanguageDTO> ret = new ArrayList<LanguageDTO>();		
		for(Language language : languageRepository.findAll())
		{
			ret.add(new LanguageDTO(language.getLanguageId(), language.getName(), language.getLastUpdate()));
			
		}
		return ret;
	}
	
	@Transactional
	public void save(LanguageDTO languageDTO) 
	{
		Language language = new Language(languageDTO.getName(), languageDTO.getLastUpdate());
		languageRepository.save(language);		
	}
	
	@Transactional
	public LanguageDTO update(short id, LanguageDTO languageDTO) throws javax.persistence.RollbackException   
	{		
		Language language = languageRepository.findOne(id);
		
		if(language != null)
		{					
			language.setName(languageDTO.getName());
			language.setLastUpdate(new Date());			
			LanguageDTO ret = new LanguageDTO(language.getLanguageId(), language.getName(), language.getLastUpdate());		
			return ret;		
		}
		else
		{
			return null;		
		}			
	}
	
	@Transactional
	public Short delete(short id) throws javax.persistence.RollbackException   
	{		
		Language language = languageRepository.findOne(id);
		
		if(language != null)
		{
			languageRepository.delete(language);
			return id;
		}
		else
		{
			return null;		
		}			
	}
	
	@Transactional
	public void deleteAll() 
	{		
		languageRepository.deleteAll();		
	}
	
	

}
