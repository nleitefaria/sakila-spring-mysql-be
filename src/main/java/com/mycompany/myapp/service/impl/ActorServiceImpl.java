package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.ActorDTO;
import com.mycompany.myapp.entity.Actor;
import com.mycompany.myapp.repository.ActorRepository;
import com.mycompany.myapp.service.ActorService;

@Service
public class ActorServiceImpl implements ActorService
{
	@Autowired
	ActorRepository actorRepository;

	@Transactional
	public long count() 
	{		
		return actorRepository.count();		
	}
	
	@Transactional
	public ActorDTO findOne(Short id) 
	{
		Actor actor = actorRepository.findOne(id);	
		return new ActorDTO(actor.getActorId(), actor.getFirstName(), actor.getLastName(), actor.getLastUpdate());
	}

	@Transactional
	public List<ActorDTO> findAll() 
	{	
		List<ActorDTO> ret = new ArrayList<ActorDTO>();		
		for(Actor actor : actorRepository.findAll())
		{
			ret.add(new ActorDTO(actor.getActorId(), actor.getFirstName(), actor.getLastName(), actor.getLastUpdate()));
			
		}
		return ret;
	}
	
	@Transactional
	public void save(ActorDTO actorDTO) 
	{	
		Actor actor = new Actor(actorDTO.getFirstName(), actorDTO.getLastName(), actorDTO.getLastUpdate());
		actorRepository.save(actor);		
	}
	
	@Transactional
	public ActorDTO update(short id, ActorDTO actorDTO) throws javax.persistence.RollbackException   
	{		
		Actor actor = actorRepository.findOne(id);
		
		if(actor != null)
		{
			actor.setFirstName(actorDTO.getFirstName());
			actor.setLastName(actorDTO.getLastName());
			actor.setLastUpdate(new Date());
			ActorDTO ret = new ActorDTO(actor.getActorId(), actor.getFirstName(), actor.getLastName(), new Date());
			return ret;
		}
		else
		{
			return null;		
		}			
	}
	
	@Transactional
	public Short delete(short id) throws javax.persistence.RollbackException   
	{		
		Actor actor = actorRepository.findOne(id);
		
		if(actor != null)
		{
			actorRepository.delete(actor);
			return id;
		}
		else
		{
			return null;		
		}			
	}
	
	@Transactional
	public void deleteAll() 
	{		
		actorRepository.deleteAll();		
	}	
}
