package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.CityDTO;
import com.mycompany.myapp.domain.CountryDTO;
import com.mycompany.myapp.entity.City;
import com.mycompany.myapp.entity.Country;
import com.mycompany.myapp.repository.CityRepository;
import com.mycompany.myapp.repository.CountryRepository;
import com.mycompany.myapp.service.CityService;

@Service
public class CityServiceImpl implements CityService 
{
	@Autowired
	CityRepository cityRepository;
	
	@Autowired
	CountryRepository countryRepository;

	@Transactional
	public CityDTO findOne(Short id) 
	{
		City city = cityRepository.findOne(id);		
		return new CityDTO(city.getCityId(), new CountryDTO(city.getCountry().getCountryId(), city.getCountry().getCountry(), city.getCountry().getLastUpdate()), city.getCity(), city.getLastUpdate());
	}

	@Transactional
	public List<CityDTO> findAll() 
	{			
		List<CityDTO> ret = new ArrayList<CityDTO>();		
		for(City city : cityRepository.findAll())
		{
			ret.add(new CityDTO(city.getCityId(), new CountryDTO(city.getCountry().getCountryId(), city.getCountry().getCountry(), city.getCountry().getLastUpdate()), city.getCity(), city.getLastUpdate()));		
		}
		return ret;
	}
	
	@Transactional
	public void save(CityDTO cityDTO) 
	{
		Country country = countryRepository.findOne(cityDTO.getCountry().getCountryId());
		
		if(country == null)
		{
			country = new Country(cityDTO.getCountry().getCountry(), new Date());
			countryRepository.save(country);
		}
		
		City city = new City(country, cityDTO.getCity(), new Date());
		cityRepository.save(city);		
	}
	
	@Transactional
	public CityDTO update(short id, CityDTO cityDTO) throws javax.persistence.RollbackException   
	{		
		City city = cityRepository.findOne(id);
		
		if(city != null)
		{					
			city.setCity(cityDTO.getCity());
			city.setLastUpdate(new Date());			
			CountryDTO countryDTO = new CountryDTO(city.getCountry().getCountryId(), city.getCountry().getCountry(), city.getCountry().getLastUpdate());			
			CityDTO ret = new CityDTO(city.getCityId(), countryDTO, city.getCity(), city.getLastUpdate());
			return ret;		
		}
		else
		{
			return null;		
		}			
	}
	
	@Transactional
	public Short delete(short id) throws javax.persistence.RollbackException   
	{		
		City city = cityRepository.findOne(id);		
		if(city != null)
		{
			cityRepository.delete(city);
			return id;
		}
		else
		{
			return null;		
		}			
	}
	
	@Transactional
	public void deleteAll() 
	{		
		cityRepository.deleteAll();		
	}
	
	

}
