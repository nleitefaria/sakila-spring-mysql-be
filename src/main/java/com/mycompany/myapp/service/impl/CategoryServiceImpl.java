package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.CategoryDTO;
import com.mycompany.myapp.entity.Category;
import com.mycompany.myapp.repository.CategoryRepository;
import com.mycompany.myapp.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService
{
	@Autowired
	CategoryRepository categoryRepository;

	@Transactional
	public long count() 
	{		
		return categoryRepository.count();		
	}
	
	@Transactional
	public CategoryDTO findOne(Short id) 
	{
		Category category = categoryRepository.findOne(id);	
		return new CategoryDTO(category.getCategoryId(), category.getName(), category.getLastUpdate());
	}
	
	@Transactional
	public List<CategoryDTO> findAll() 
	{				
		List<CategoryDTO> ret = new ArrayList<CategoryDTO>();		
		for(Category category : categoryRepository.findAll())
		{
			ret.add(new CategoryDTO(category.getCategoryId(), category.getName(), category.getLastUpdate()));
		}		
		return ret;		
	}
	
	@Transactional
	public void save(CategoryDTO categoryDTO) 
	{
		Category category = new Category(categoryDTO.getName(), categoryDTO.getLastUpdate());
		categoryRepository.save(category);		
	}
	
	@Transactional
	public CategoryDTO update(short id, CategoryDTO categoryDTO) throws javax.persistence.RollbackException   
	{		
		Category category = categoryRepository.findOne(id);
		
		if(category != null)
		{
			category.setName(categoryDTO.getName());			
			category.setLastUpdate(new Date());

			CategoryDTO ret = new CategoryDTO(category.getCategoryId(), category.getName(), category.getLastUpdate());
			return ret;
		}
		else
		{
			return null;		
		}			
	}
	
	@Transactional
	public Short delete(short id) throws javax.persistence.RollbackException   
	{		
		Category category = categoryRepository.findOne(id);
		
		if(category != null)
		{
			categoryRepository.delete(category);
			return id;
		}
		else
		{
			return null;		
		}			
	}
	
	@Transactional
	public void deleteAll() 
	{		
		categoryRepository.deleteAll();		
	}

}
