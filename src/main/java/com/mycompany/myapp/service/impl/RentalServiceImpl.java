package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.AddressDTO;
import com.mycompany.myapp.domain.CityDTO;
import com.mycompany.myapp.domain.CountryDTO;
import com.mycompany.myapp.domain.CustomerDTO;
import com.mycompany.myapp.domain.FilmDTO;
import com.mycompany.myapp.domain.InventoryDTO;
import com.mycompany.myapp.domain.RentalDTO;
import com.mycompany.myapp.domain.StaffDTO;
import com.mycompany.myapp.domain.StoreDTO;
import com.mycompany.myapp.entity.Address;
import com.mycompany.myapp.entity.Customer;
import com.mycompany.myapp.entity.Film;
import com.mycompany.myapp.entity.Inventory;
import com.mycompany.myapp.entity.Rental;
import com.mycompany.myapp.entity.Staff;
import com.mycompany.myapp.entity.Store;
import com.mycompany.myapp.repository.RentalRepository;
import com.mycompany.myapp.service.RentalService;

@Service
public class RentalServiceImpl implements RentalService
{
	@Autowired
	RentalRepository rentalRepository;

	@Transactional
	public long count() 
	{		
		return rentalRepository.count();		
	}
	
	@Transactional
	public RentalDTO findOne(Integer id) 
	{
		Rental rental = rentalRepository.findOne(id);						
		Customer customer = rental.getCustomer();	
		CountryDTO countryStoreDTO = new CountryDTO(customer.getStore().getAddress().getCity().getCountry().getCountry(), customer.getStore().getAddress().getCity().getCountry().getLastUpdate());		
		CityDTO cityStoreDTO = new CityDTO(countryStoreDTO, customer.getStore().getAddress().getCity().getCity(), customer.getStore().getAddress().getCity().getLastUpdate());				
		AddressDTO addressCustomerDTO = new AddressDTO(customer.getAddress().getAddressId(), cityStoreDTO, customer.getStore().getAddress().getAddress(), customer.getStore().getAddress().getAddress2(), customer.getStore().getAddress().getDistrict(), customer.getStore().getAddress().getPostalCode(), customer.getStore().getAddress().getPhone(), customer.getStore().getAddress().getLastUpdate());		
		AddressDTO addressStoreDTO = new AddressDTO(customer.getStore().getAddress().getAddressId(), cityStoreDTO, customer.getStore().getAddress().getAddress(), customer.getStore().getAddress().getAddress2(), customer.getStore().getAddress().getDistrict(), customer.getStore().getAddress().getPostalCode(), customer.getStore().getAddress().getPhone(), customer.getStore().getAddress().getLastUpdate());
		StaffDTO staffStoreDTO = new StaffDTO(customer.getStore().getStaff().getStaffId(), customer.getStore().getStaff().getFirstName(), customer.getStore().getStaff().getLastName(), customer.getStore().getStaff().getPicture(), customer.getStore().getStaff().getEmail(), customer.getStore().getStaff().isActive(), customer.getStore().getStaff().getUsername(), customer.getStore().getStaff().getPassword(), customer.getStore().getStaff().getLastUpdate());		
		StoreDTO storeDTO = new StoreDTO(customer.getStore().getStoreId(), addressStoreDTO, staffStoreDTO, customer.getStore().getLastUpdate());			
		CustomerDTO customerDTO =  new CustomerDTO(customer.getCustomerId(), addressCustomerDTO, storeDTO, customer.getFirstName(), customer.getLastName(), customer.getEmail(), customer.isActive(), customer.getCreateDate(), customer.getLastUpdate());
		Inventory inventory = rental.getInventory();		
		Film film = rental.getInventory().getFilm();
		FilmDTO filmDTO = new FilmDTO(film.getFilmId(), film.getTitle(), film.getDescription(), film.getReleaseYear(), film.getRentalDuration(), film.getRentalRate(), film.getLength(), film.getReplacementCost(), film.getRating(), film.getSpecialFeatures(), film.getLastUpdate());		
		InventoryDTO inventoryDTO =  new InventoryDTO(inventory.getInventoryId(), filmDTO, storeDTO, inventory.getLastUpdate());	
		StaffDTO staffDTO = new StaffDTO(rental.getStaff().getStaffId() , rental.getStaff().getFirstName(), rental.getStaff().getLastName(),rental.getStaff().getPicture(), rental.getStaff().getEmail(), rental.getStaff().isActive(), rental.getStaff().getUsername(), rental.getStaff().getPassword(), rental.getStaff().getLastUpdate());
		return new RentalDTO(rental.getRentalId(), customerDTO, inventoryDTO, staffDTO, rental.getRentalDate(), rental.getReturnDate(), rental.getLastUpdate());
	}
	
	
	@Transactional
	public List<RentalDTO> findAll() 
	{	
		List<RentalDTO> ret = new ArrayList<RentalDTO>();		
		for(Rental rental : rentalRepository.findAll())
		{		
			Customer customer = rental.getCustomer();	
			Address address = customer.getAddress();		
			CityDTO city = new CityDTO(address.getCity().getCityId(), new CountryDTO(address.getCity().getCountry().getCountryId(), address.getCity().getCountry().getCountry(), address.getCity().getCountry().getLastUpdate()), address.getCity().getCity(), address.getCity().getLastUpdate());		
			CountryDTO countryStoreDTO = new CountryDTO(customer.getStore().getAddress().getCity().getCountry().getCountry(), customer.getStore().getAddress().getCity().getCountry().getLastUpdate());			
			CityDTO cityStoreDTO = new CityDTO(countryStoreDTO, customer.getStore().getAddress().getCity().getCity(), customer.getStore().getAddress().getCity().getLastUpdate());				
			AddressDTO addressCustomerDTO = new AddressDTO(customer.getAddress().getAddressId(), cityStoreDTO, customer.getStore().getAddress().getAddress(), customer.getStore().getAddress().getAddress2(), customer.getStore().getAddress().getDistrict(), customer.getStore().getAddress().getPostalCode(), customer.getStore().getAddress().getPhone(), customer.getStore().getAddress().getLastUpdate());		
			AddressDTO addressStoreDTO = new AddressDTO(customer.getStore().getAddress().getAddressId(), cityStoreDTO, customer.getStore().getAddress().getAddress(), customer.getStore().getAddress().getAddress2(), customer.getStore().getAddress().getDistrict(), customer.getStore().getAddress().getPostalCode(), customer.getStore().getAddress().getPhone(), customer.getStore().getAddress().getLastUpdate());
			StaffDTO staffStoreDTO = new StaffDTO(customer.getStore().getStaff().getStaffId(), customer.getStore().getStaff().getFirstName(), customer.getStore().getStaff().getLastName(), customer.getStore().getStaff().getPicture(), customer.getStore().getStaff().getEmail(), customer.getStore().getStaff().isActive(), customer.getStore().getStaff().getUsername(), customer.getStore().getStaff().getPassword(), customer.getStore().getStaff().getLastUpdate());			
			StoreDTO storeCustomerDTO = new StoreDTO(customer.getStore().getStoreId(), addressStoreDTO, staffStoreDTO, customer.getStore().getLastUpdate());				
			CustomerDTO customerDTO =  new CustomerDTO(customer.getCustomerId(), addressCustomerDTO, storeCustomerDTO, customer.getFirstName(), customer.getLastName(), customer.getEmail(), customer.isActive(), customer.getCreateDate(), customer.getLastUpdate());
			Inventory inventory = rental.getInventory();		
			Film film = rental.getInventory().getFilm();
			FilmDTO filmDTO = new FilmDTO(film.getFilmId(), film.getTitle(), film.getDescription(), film.getReleaseYear(), film.getRentalDuration(), film.getRentalRate(), film.getLength(), film.getReplacementCost(), film.getRating(), film.getSpecialFeatures(), film.getLastUpdate());		
			Address addressStore = inventory.getStore().getAddress();		
			Store store = inventory.getStore();	
			Staff staff = inventory.getStore().getStaff();
			StoreDTO storeDTO = new StoreDTO(store.getStoreId() , new AddressDTO(addressStore.getAddressId(), city, addressStore.getAddress(), addressStore.getAddress2(), addressStore.getDistrict(), addressStore.getPostalCode(), addressStore.getPhone(), addressStore.getLastUpdate()), new StaffDTO(staff.getStaffId(), staff.getFirstName(),staff.getLastName(), staff.getPicture(), staff.getEmail(), staff.isActive(), staff.getUsername(), staff.getPassword(), staff.getLastUpdate()),store.getLastUpdate());		
			InventoryDTO inventoryDTO =  new InventoryDTO(inventory.getInventoryId(), filmDTO, storeDTO, inventory.getLastUpdate());
			StaffDTO staffDTO = new StaffDTO(rental.getStaff().getStaffId() , rental.getStaff().getFirstName(), rental.getStaff().getLastName(),rental.getStaff().getPicture(), rental.getStaff().getEmail(), rental.getStaff().isActive(), rental.getStaff().getUsername(), rental.getStaff().getPassword(), rental.getStaff().getLastUpdate());
			ret.add(new RentalDTO(rental.getRentalId(), customerDTO, inventoryDTO, staffDTO, rental.getRentalDate(), rental.getReturnDate(), rental.getLastUpdate()));		
		}
		return ret;
	}
	
	
}



