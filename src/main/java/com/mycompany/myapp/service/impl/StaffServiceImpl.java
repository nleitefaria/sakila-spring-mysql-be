package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mycompany.myapp.domain.StaffDTO;
import com.mycompany.myapp.entity.Staff;
import com.mycompany.myapp.repository.StaffRepository;
import com.mycompany.myapp.service.StaffService;

@Service
public class StaffServiceImpl implements StaffService
{
	@Autowired
	StaffRepository staffRepository;

	@Transactional
	public long count() 
	{		
		return staffRepository.count();		
	}
	
	@Transactional
	public StaffDTO findOne(Short id) 
	{
		Staff staff = staffRepository.findOne(id);			
		return new StaffDTO(staff.getStaffId(), staff.getFirstName(), staff.getLastName(), staff.getPicture(), staff.getEmail(), staff.isActive(), staff.getUsername(), staff.getPassword(), staff.getLastUpdate());
	}
	
	@Transactional
	public List<StaffDTO> findAll() 
	{
		List<StaffDTO> ret = new ArrayList<StaffDTO>();		
		for(Staff staff : staffRepository.findAll())
		{
			ret.add(new StaffDTO(staff.getStaffId(), staff.getFirstName(), staff.getLastName(), staff.getPicture(), staff.getEmail(), staff.isActive(), staff.getUsername(), staff.getPassword(), staff.getLastUpdate()));		
		}
		return ret;
		
	}

}
