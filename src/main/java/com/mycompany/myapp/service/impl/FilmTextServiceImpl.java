package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.FilmTextDTO;
import com.mycompany.myapp.entity.FilmText;
import com.mycompany.myapp.repository.FilmTextRepository;
import com.mycompany.myapp.service.FilmTextService;

@Service
public class FilmTextServiceImpl implements FilmTextService
{	
	@Autowired
	FilmTextRepository filmTextRepository;

	
	@Transactional
	public long count() 
	{		
		return filmTextRepository.count();		
	}
	
	@Transactional
	public FilmTextDTO findOne(Short id) 
	{
		FilmText filmText = filmTextRepository.findOne(id);	
		return new FilmTextDTO(filmText.getFilmId(), filmText.getTitle(), filmText.getDescription());
	}

	@Transactional
	public List<FilmTextDTO> findAll() 
	{				
		List<FilmTextDTO> ret = new ArrayList<FilmTextDTO>();		
		for(FilmText filmText : filmTextRepository.findAll())
		{
			ret.add(new FilmTextDTO(filmText.getFilmId(), filmText.getTitle(), filmText.getDescription()));
		}		
		return ret;		
	}
	
	@Transactional
	public void save(FilmTextDTO filmTextDTO) 
	{
		FilmText filmText = new FilmText(filmTextDTO.getFilmId(), filmTextDTO.getTitle(), filmTextDTO.getDescription());
		filmTextRepository.save(filmText);		
	}
	
	
	@Transactional
	public FilmTextDTO update(short id, FilmTextDTO filmTextDTO) throws javax.persistence.RollbackException   
	{		
		FilmText filmText = filmTextRepository.findOne(id);
		
		if(filmText != null)
		{
			filmText.setTitle(filmTextDTO.getTitle());
			filmText.setDescription(filmTextDTO.getDescription());
			
			FilmTextDTO ret = new FilmTextDTO(filmText.getFilmId(), filmText.getTitle(),  filmText.getDescription());		
			return ret;
		}
		else
		{
			return null;		
		}			
	}
	
	
	@Transactional
	public Short delete(short id) throws javax.persistence.RollbackException   
	{		
		FilmText filmText = filmTextRepository.findOne(id);
		
		if(filmText != null)
		{
			filmTextRepository.delete(filmText);
			return id;
		}
		else
		{
			return null;		
		}			
	}
	
	@Transactional
	public void deleteAll() 
	{		
		filmTextRepository.deleteAll();		
	}
}
