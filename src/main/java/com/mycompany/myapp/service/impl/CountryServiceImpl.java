package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.CountryDTO;
import com.mycompany.myapp.entity.Country;
import com.mycompany.myapp.repository.CountryRepository;
import com.mycompany.myapp.service.CountryService;

@Service
public class CountryServiceImpl implements CountryService
{	
	@Autowired
	CountryRepository countryRepository;

	@Transactional
	public CountryDTO findOne(Short id) 
	{
		Country country = countryRepository.findOne(id);
		return new CountryDTO(country.getCountryId(), country.getCountry(), country.getLastUpdate());
	}

	@Transactional
	public List<CountryDTO> findAll() 
	{		
		List<CountryDTO> ret = new ArrayList<CountryDTO>();
		for(Country country : countryRepository.findAll())
		{
			ret.add(new CountryDTO(country.getCountryId(), country.getCountry(), country.getLastUpdate()));			
		}		
		return ret;
	}
	
	@Transactional
	public void save(CountryDTO countryDTO) 
	{
		Country country = new Country(countryDTO.getCountry(), countryDTO.getLastUpdate());
		countryRepository.save(country);		
	}
	
	@Transactional
	public CountryDTO update(short id, CountryDTO countryDTO) throws javax.persistence.RollbackException   
	{		
		Country country = countryRepository.findOne(id);
			
		if(country != null)
		{			
				country.setCountry(countryDTO.getCountry());
				country.setLastUpdate(new Date());
				CountryDTO ret = new CountryDTO(country.getCountry(), country.getLastUpdate());
				return ret;
		}
		else
		{
			return null;		
		}			
	}
	
	@Transactional
	public Short delete(short id) throws javax.persistence.RollbackException   
	{		
		Country country = countryRepository.findOne(id);
		
		if(country != null)
		{
			countryRepository.delete(country);
			return id;
		}
		else
		{
			return null;		
		}			
	}
	
	@Transactional
	public void deleteAll() 
	{		
		countryRepository.deleteAll();		
	}
}
