package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.AddressDTO;
import com.mycompany.myapp.domain.CityDTO;
import com.mycompany.myapp.domain.CountryDTO;
import com.mycompany.myapp.domain.FilmDTO;
import com.mycompany.myapp.domain.InventoryDTO;
import com.mycompany.myapp.domain.StaffDTO;
import com.mycompany.myapp.domain.StoreDTO;
import com.mycompany.myapp.entity.Address;
import com.mycompany.myapp.entity.Film;
import com.mycompany.myapp.entity.Inventory;
import com.mycompany.myapp.entity.Staff;
import com.mycompany.myapp.entity.Store;
import com.mycompany.myapp.repository.InventoryRepository;
import com.mycompany.myapp.service.InventoryService;

@Service
public class InventoryServiceImpl implements InventoryService
{
	@Autowired
	InventoryRepository inventoryRepository;

	@Transactional
	public long count() 
	{		
		return inventoryRepository.count();		
	}
	
	@Transactional
	public InventoryDTO findOne(Integer id) 
	{
		Inventory inventory = inventoryRepository.findOne(id);			
		Film film = inventory.getFilm();		
		FilmDTO filmDTO = new FilmDTO(film.getFilmId(), film.getTitle(), film.getDescription(), film.getReleaseYear(), film.getRentalDuration(), film.getRentalRate(), film.getLength(), film.getReplacementCost(), film.getRating(), film.getSpecialFeatures(), film.getLastUpdate());		
		Address address = inventory.getStore().getAddress();		
		CityDTO city = new CityDTO(address.getCity().getCityId(), new CountryDTO(address.getCity().getCountry().getCountryId(), address.getCity().getCountry().getCountry(), address.getCity().getCountry().getLastUpdate()), address.getCity().getCity(), address.getCity().getLastUpdate());
		//AddressDTO addressDTO = new AddressDTO(address.getAddressId(), city, address.getAddress(), address.getAddress2(), address.getDistrict(), address.getPostalCode(), address.getPhone(), address.getLastUpdate());
		Store store = inventory.getStore();	
		Staff staff = inventory.getStore().getStaff();
		StoreDTO storeDTO = new StoreDTO(store.getStoreId() , new AddressDTO(address.getAddressId(), city, address.getAddress(), address.getAddress2(), address.getDistrict(), address.getPostalCode(), address.getPhone(), address.getLastUpdate()), new StaffDTO(staff.getStaffId(), staff.getFirstName(),staff.getLastName(), staff.getPicture(), staff.getEmail(), staff.isActive(), staff.getUsername(), staff.getPassword(), staff.getLastUpdate()),store.getLastUpdate());		
		return new InventoryDTO(inventory.getInventoryId(), filmDTO, storeDTO, inventory.getLastUpdate());
	}
	
	@Transactional
	public List<InventoryDTO> findAll() 
	{			
		List<InventoryDTO> ret = new ArrayList<InventoryDTO>();		
		for(Inventory inventory : inventoryRepository.findAll())
		{
			Film film = inventory.getFilm();		
			FilmDTO filmDTO = new FilmDTO(film.getFilmId(), film.getTitle(), film.getDescription(), film.getReleaseYear(), film.getRentalDuration(), film.getRentalRate(), film.getLength(), film.getReplacementCost(), film.getRating(), film.getSpecialFeatures(), film.getLastUpdate());		
			Address address = inventory.getStore().getAddress();		
			CityDTO city = new CityDTO(address.getCity().getCityId(), new CountryDTO(address.getCity().getCountry().getCountryId(), address.getCity().getCountry().getCountry(), address.getCity().getCountry().getLastUpdate()), address.getCity().getCity(), address.getCity().getLastUpdate());
			Store store = inventory.getStore();	
			Staff staff = inventory.getStore().getStaff();
			StoreDTO storeDTO = new StoreDTO(store.getStoreId() , new AddressDTO(address.getAddressId(), city, address.getAddress(), address.getAddress2(), address.getDistrict(), address.getPostalCode(), address.getPhone(), address.getLastUpdate()), new StaffDTO(staff.getStaffId(), staff.getFirstName(),staff.getLastName(), staff.getPicture(), staff.getEmail(), staff.isActive(), staff.getUsername(), staff.getPassword(), staff.getLastUpdate()),store.getLastUpdate());		
			ret.add(new InventoryDTO(inventory.getInventoryId(), filmDTO, storeDTO, inventory.getLastUpdate()));		
		}
		return ret;
	}

}
