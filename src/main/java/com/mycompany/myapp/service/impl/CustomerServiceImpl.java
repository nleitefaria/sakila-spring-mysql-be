package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.AddressDTO;
import com.mycompany.myapp.domain.CityDTO;
import com.mycompany.myapp.domain.CountryDTO;
import com.mycompany.myapp.domain.CustomerDTO;
import com.mycompany.myapp.domain.StaffDTO;
import com.mycompany.myapp.domain.StoreDTO;
import com.mycompany.myapp.entity.Address;
import com.mycompany.myapp.entity.Customer;
import com.mycompany.myapp.entity.Store;
import com.mycompany.myapp.repository.AddressRepository;
import com.mycompany.myapp.repository.CityRepository;
import com.mycompany.myapp.repository.CountryRepository;
import com.mycompany.myapp.repository.CustomerRepository;
import com.mycompany.myapp.repository.StoreRepository;
import com.mycompany.myapp.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService
{	
	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	AddressRepository addressRepository;
	
	@Autowired
	StoreRepository storeRepository;
	
	@Autowired
	CityRepository cityRepository;
	
	@Autowired
	CountryRepository countryRepository;
	
	@Transactional
	public long count() 
	{		
		return customerRepository.count();		
	}
	
	@Transactional
	public CustomerDTO findOne(Short id) 
	{
		Customer customer = customerRepository.findOne(id);		
		
		CountryDTO countryStoreDTO = new CountryDTO(customer.getStore().getAddress().getCity().getCountry().getCountry(), customer.getStore().getAddress().getCity().getCountry().getLastUpdate());
		CityDTO cityStoreDTO = new CityDTO(countryStoreDTO, customer.getStore().getAddress().getCity().getCity(), customer.getStore().getAddress().getCity().getLastUpdate());		
		AddressDTO addressStoreDTO = new AddressDTO(customer.getStore().getAddress().getAddressId(), cityStoreDTO, customer.getStore().getAddress().getAddress(), customer.getStore().getAddress().getAddress2(), customer.getStore().getAddress().getDistrict(), customer.getStore().getAddress().getPostalCode(), customer.getStore().getAddress().getPhone(), customer.getStore().getAddress().getLastUpdate());
		StaffDTO staffStoreDTO = new StaffDTO(customer.getStore().getStaff().getStaffId(), customer.getStore().getStaff().getFirstName(), customer.getStore().getStaff().getLastName(), customer.getStore().getStaff().getPicture(), customer.getStore().getStaff().getEmail(), customer.getStore().getStaff().isActive(), customer.getStore().getStaff().getUsername(), customer.getStore().getStaff().getPassword(), customer.getStore().getStaff().getLastUpdate());
		StoreDTO storeDTO = new StoreDTO(customer.getStore().getStoreId(), addressStoreDTO, staffStoreDTO, customer.getStore().getLastUpdate());	
		AddressDTO addressCustomerDTO = new AddressDTO(customer.getAddress().getAddressId(), cityStoreDTO, customer.getStore().getAddress().getAddress(), customer.getStore().getAddress().getAddress2(), customer.getStore().getAddress().getDistrict(), customer.getStore().getAddress().getPostalCode(), customer.getStore().getAddress().getPhone(), customer.getStore().getAddress().getLastUpdate());
		
		return new CustomerDTO(customer.getCustomerId(), addressCustomerDTO, storeDTO, customer.getFirstName(), customer.getLastName(), customer.getEmail(), customer.isActive(), customer.getCreateDate(), customer.getLastUpdate());
	}
	
	@Transactional
	public List<CustomerDTO> findAll() 
	{	
		List<CustomerDTO> ret = new ArrayList<CustomerDTO>();		
		for(Customer customer : customerRepository.findAll())
		{
			CountryDTO countryStoreDTO = new CountryDTO(customer.getStore().getAddress().getCity().getCountry().getCountry(), customer.getStore().getAddress().getCity().getCountry().getLastUpdate());
			CityDTO cityStoreDTO = new CityDTO(countryStoreDTO, customer.getStore().getAddress().getCity().getCity(), customer.getStore().getAddress().getCity().getLastUpdate());		
			AddressDTO addressStoreDTO = new AddressDTO(customer.getStore().getAddress().getAddressId(), cityStoreDTO, customer.getStore().getAddress().getAddress(), customer.getStore().getAddress().getAddress2(), customer.getStore().getAddress().getDistrict(), customer.getStore().getAddress().getPostalCode(), customer.getStore().getAddress().getPhone(), customer.getStore().getAddress().getLastUpdate());
			StaffDTO staffStoreDTO = new StaffDTO(customer.getStore().getStaff().getStaffId(), customer.getStore().getStaff().getFirstName(), customer.getStore().getStaff().getLastName(), customer.getStore().getStaff().getPicture(), customer.getStore().getStaff().getEmail(), customer.getStore().getStaff().isActive(), customer.getStore().getStaff().getUsername(), customer.getStore().getStaff().getPassword(), customer.getStore().getStaff().getLastUpdate());
			StoreDTO storeDTO = new StoreDTO(customer.getStore().getStoreId(), addressStoreDTO, staffStoreDTO, customer.getStore().getLastUpdate());	
			AddressDTO addressCustomerDTO = new AddressDTO(customer.getAddress().getAddressId(), cityStoreDTO, customer.getStore().getAddress().getAddress(), customer.getStore().getAddress().getAddress2(), customer.getStore().getAddress().getDistrict(), customer.getStore().getAddress().getPostalCode(), customer.getStore().getAddress().getPhone(), customer.getStore().getAddress().getLastUpdate());		
			ret.add(new CustomerDTO(customer.getCustomerId(), addressCustomerDTO, storeDTO, customer.getFirstName(), customer.getLastName(), customer.getEmail(), customer.isActive(), customer.getCreateDate(), customer.getLastUpdate()));		
		}
		return ret;
	}
	
	@Transactional
	public void save(CustomerDTO customerDTO)
	{
		Address address = addressRepository.findOne(customerDTO.getAddress().getAddressId());
		Store store = storeRepository.findOne(customerDTO.getStore().getStoreId());		
		Customer customer = new Customer(address, store, customerDTO.getFirstName(), customerDTO.getLastName(), customerDTO.getEmail(), customerDTO.isActive(), customerDTO.getCreateDate(), customerDTO.getLastUpdate());
		customerRepository.save(customer);		
	}
	
	
}
