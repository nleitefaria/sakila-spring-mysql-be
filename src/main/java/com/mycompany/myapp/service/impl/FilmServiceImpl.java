package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.FilmDTO;
import com.mycompany.myapp.entity.Film;
import com.mycompany.myapp.repository.FilmRepository;
import com.mycompany.myapp.service.FilmService;

@Service
public class FilmServiceImpl implements FilmService 
{
	@Autowired
	FilmRepository filmRepository;
	
	@Transactional
	public long count() 
	{		
		return filmRepository.count();		
	}
	
	@Transactional
	public FilmDTO findOne(Short id) 
	{
		Film film = filmRepository.findOne(id);	
		return new FilmDTO(film.getFilmId(), film.getTitle(), film.getDescription(), film.getReleaseYear(), film.getRentalDuration(), film.getRentalRate(), film.getLength(), film.getReplacementCost(), film.getRating(), film.getSpecialFeatures(), film.getLastUpdate());	
	}
	
	@Transactional
	public List<FilmDTO> findAll() 
	{	
		List<FilmDTO> ret = new ArrayList<FilmDTO>();		
		for(Film film : filmRepository.findAll())
		{
			ret.add(new FilmDTO(film.getFilmId(), film.getTitle(), film.getDescription(), film.getReleaseYear(), film.getRentalDuration(), film.getRentalRate(), film.getLength(), film.getReplacementCost(), film.getRating(), film.getSpecialFeatures(), film.getLastUpdate()));			
		}
		return ret;
	}

}
