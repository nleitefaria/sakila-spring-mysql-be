package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.StoreDTO;

public interface StoreService 
{
	long count();
	StoreDTO findOne(Short id);
	List<StoreDTO> findAll();
	void save(StoreDTO storeDTO);
	StoreDTO update(short id, StoreDTO storeDTO) throws javax.persistence.RollbackException;
}
