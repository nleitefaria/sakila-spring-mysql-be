package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.CityDTO;

public interface CityService
{
	CityDTO findOne(Short id);
	List<CityDTO> findAll();
	void save(CityDTO city);
	CityDTO update(short id, CityDTO cityDTO) throws javax.persistence.RollbackException;
	Short delete(short id) throws javax.persistence.RollbackException;
	void deleteAll();	
}
