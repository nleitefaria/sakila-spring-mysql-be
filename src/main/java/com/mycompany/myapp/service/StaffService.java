package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.StaffDTO;

public interface StaffService 
{
	long count(); 
	StaffDTO findOne(Short id);
	List<StaffDTO> findAll();
}
