package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.FilmTextDTO;

public interface FilmTextService
{	
	long count();
	FilmTextDTO findOne(Short id);
	List<FilmTextDTO> findAll();
	void save(FilmTextDTO filmTextDTO);
	FilmTextDTO update(short id, FilmTextDTO filmTextDTO) throws javax.persistence.RollbackException;
	Short delete(short id) throws javax.persistence.RollbackException;
	void deleteAll();
}
