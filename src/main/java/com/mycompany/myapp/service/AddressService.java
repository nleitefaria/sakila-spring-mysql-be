package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.AddressDTO;

public interface AddressService 
{
	AddressDTO findOne(Short id);
	List<AddressDTO> findAll();
	void save(AddressDTO addressDTO);

}
