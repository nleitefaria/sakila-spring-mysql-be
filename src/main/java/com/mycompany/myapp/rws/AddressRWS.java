package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.AddressDTO;
import com.mycompany.myapp.service.AddressService;

@RestController
public class AddressRWS 
{	
	@Autowired
	AddressService addressService;
	
	private static final Logger logger = LoggerFactory.getLogger(AddressRWS.class);
	
	@RequestMapping(value = "/address/{id}", method = RequestMethod.GET)
	public ResponseEntity<AddressDTO> findOne(@PathVariable Short id) {
		logger.info("Listing address with id: " + id);
		return new ResponseEntity<AddressDTO>(addressService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addresses", method = RequestMethod.GET)
	public ResponseEntity<List<AddressDTO>> findAll() {
		logger.info("Listing all addresses");
		return new ResponseEntity<List<AddressDTO>>(addressService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/address", method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody AddressDTO addressDTO)
	{       
        logger.info("Creating address"); 	
		try
		{
			addressService.save(addressDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
    }

}
