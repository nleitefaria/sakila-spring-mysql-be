package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.CustomerDTO;
import com.mycompany.myapp.service.CustomerService;

@RestController
public class CustomerRWS 
{	
	@Autowired
	CustomerService customerService;
	
	private static final Logger logger = LoggerFactory.getLogger(CustomerRWS.class);
	
	@RequestMapping(value = "/customer/{id}", method = RequestMethod.GET)
	public ResponseEntity<CustomerDTO> findOne(@PathVariable Short id)
	{
		logger.info("Listing customer with id: " + id);
		return new ResponseEntity<CustomerDTO>(customerService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/customer", method = RequestMethod.GET)
	public ResponseEntity<List<CustomerDTO>> findAll() {
		logger.info("Listing all customers");
		return new ResponseEntity<List<CustomerDTO>>(customerService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/customer", method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody CustomerDTO customerDTO)
	{       
	        logger.info("Creating customer"); 	
		try
		{
			customerService.save(customerDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
	   }
	
	

}
