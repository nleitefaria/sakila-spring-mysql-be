package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.FilmTextDTO;
import com.mycompany.myapp.service.FilmTextService;

@RestController
public class FilmTextRWS 
{
	@Autowired
	FilmTextService filmTextService;
	
	private static final Logger logger = LoggerFactory.getLogger(FilmTextRWS.class);
	
	@RequestMapping(value = "/filmtext/{id}", method = RequestMethod.GET)
	public ResponseEntity<FilmTextDTO> findOne(@PathVariable Short id) {
		logger.info("Listing filmText with id: " + id);
		return new ResponseEntity<FilmTextDTO>(filmTextService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/filmtextes", method = RequestMethod.GET)
	public ResponseEntity<List<FilmTextDTO>> findAll()
	{
		logger.info("Listing all filmText");
		return new ResponseEntity<List<FilmTextDTO>>(filmTextService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/filmText", method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody FilmTextDTO filmTextDTO)
	{       
        logger.info("Creating filmText"); 	
		try
		{
			filmTextService.save(filmTextDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
    }

}
