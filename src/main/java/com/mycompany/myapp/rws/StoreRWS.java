package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.mycompany.myapp.domain.StoreDTO;
import com.mycompany.myapp.service.StoreService;

@RestController
public class StoreRWS 
{
	@Autowired
	StoreService storeService;
	
	private static final Logger logger = LoggerFactory.getLogger(StoreRWS.class);
	
	@RequestMapping(value = "/store/{id}", method = RequestMethod.GET)
	public ResponseEntity<StoreDTO> findOne(@PathVariable Short id) 
	{
		logger.info("Listing store with id: " + id);
		return new ResponseEntity<StoreDTO>(storeService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/stores", method = RequestMethod.GET)
	public ResponseEntity<List<StoreDTO>> findAll() {
		logger.info("Listing all stores");
		return new ResponseEntity<List<StoreDTO>>(storeService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/store", method = RequestMethod.POST)
	 public ResponseEntity<Void> create(@RequestBody StoreDTO storeDTO)
	{       
	        logger.info("Creating store"); 		
	        try
			{
				storeService.save(storeDTO);
				logger.info("Done");
				return new ResponseEntity<Void>(HttpStatus.CREATED);
			}
			catch(Exception e)
			{
				logger.error("An error ocurred while creating the entity" + e.getMessage());
				return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
			}	
	}

}
