package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.RentalDTO;
import com.mycompany.myapp.service.RentalService;

@RestController
public class RentalRWS
{
	private static final Logger logger = LoggerFactory.getLogger(RentalRWS.class);
	
	@Autowired
	RentalService rentalService;
	
	@RequestMapping(value = "/rental/{id}", method = RequestMethod.GET)
	public ResponseEntity<RentalDTO> findOne(@PathVariable Integer id) 
	{
		logger.info("Listing rental with id: " + id);
		return new ResponseEntity<RentalDTO>(rentalService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/rentals", method = RequestMethod.GET)
	public ResponseEntity<List<RentalDTO>> findAll()
	{
		logger.info("Listing all rentals");
		return new ResponseEntity<List<RentalDTO>>(rentalService.findAll(), HttpStatus.OK);
	}
	
	
	

}
