package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.InventoryDTO;
import com.mycompany.myapp.service.InventoryService;

@RestController
public class InventoryRWS
{
	@Autowired
	InventoryService inventoryService;
	
	private static final Logger logger = LoggerFactory.getLogger(InventoryRWS.class);
	
	@RequestMapping(value = "/inventory/{id}", method = RequestMethod.GET)
	public ResponseEntity<InventoryDTO> findOne(@PathVariable Integer id) 
	{
		logger.info("Listing inventory with id: " + id);
		return new ResponseEntity<InventoryDTO>(inventoryService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/inventory", method = RequestMethod.GET)
	public ResponseEntity<List<InventoryDTO>> findAll() 
	{
		logger.info("Listing all inventories");
		return new ResponseEntity<List<InventoryDTO>>(inventoryService.findAll(), HttpStatus.OK);
	}
	
	

}
