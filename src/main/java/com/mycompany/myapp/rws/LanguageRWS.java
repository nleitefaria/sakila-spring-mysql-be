package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.LanguageDTO;
import com.mycompany.myapp.service.LanguageService;

@RestController
public class LanguageRWS 
{
	@Autowired
	LanguageService languageService;
	
	private static final Logger logger = LoggerFactory.getLogger(LanguageRWS.class);
	
	@RequestMapping(value = "/language/{id}", method = RequestMethod.GET)
	public ResponseEntity<LanguageDTO> findOne(@PathVariable Short id) {
		logger.info("Listing language with id: " + id);
		return new ResponseEntity<LanguageDTO>(languageService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/languages", method = RequestMethod.GET)
	public ResponseEntity<List<LanguageDTO>> findAll() {
		logger.info("Listing all languages");
		return new ResponseEntity<List<LanguageDTO>>(languageService.findAll(), HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/language", method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody LanguageDTO languageDTO)
	{       
        logger.info("Creating language"); 	
		try
		{
			languageService.save(languageDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
    }
	
	@RequestMapping(value = "/language/{id}", method = RequestMethod.PUT)
	public ResponseEntity<LanguageDTO> update(@PathVariable Short id, @RequestBody LanguageDTO languageDTO) 
	{
		logger.info("Updating country with id: " + id);		
		try
		{
			LanguageDTO ret = languageService.update(id, languageDTO);
			if(ret != null)
			{
				logger.info("Done");
				return new ResponseEntity<LanguageDTO>(ret, HttpStatus.CREATED);			
			}
			else
			{
				logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<LanguageDTO>(HttpStatus.NOT_FOUND);				
			}		
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while updating the actor");
			return new ResponseEntity<LanguageDTO>(HttpStatus.BAD_REQUEST);					
		}		
	}
	
	@RequestMapping(value = "/language/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Short> delete(@PathVariable Short id)
	{       
        logger.info("Deleting entity with id: " + id);	      
        try
		{
        	Short ret = languageService.delete(id);
        	
        	if(ret != null)
        	{
        		logger.info("Done");
        		return new ResponseEntity<Short>(id, HttpStatus.NO_CONTENT);       		
        	}
        	else
        	{
        		logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<Short>(HttpStatus.NOT_FOUND);       		
        	}      	
		}
        catch(Exception e)
		{
			logger.error("An error ocurred while deleting the entity");
			return new ResponseEntity<Short>(HttpStatus.BAD_REQUEST);					
		}	     
    }


}
