package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.CategoryDTO;
import com.mycompany.myapp.service.CategoryService;

@RestController
public class CategoryRWS
{
	@Autowired
	CategoryService categoryService;
	
	private static final Logger logger = LoggerFactory.getLogger(CategoryRWS.class);
	
	@RequestMapping(value = "/category/{id}", method = RequestMethod.GET)
	public ResponseEntity<CategoryDTO> findOne(@PathVariable Short id) {
		logger.info("Listing category with id: " + id);
		return new ResponseEntity<CategoryDTO>(categoryService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	public ResponseEntity<List<CategoryDTO>> findAll() {
		logger.info("Listing all categories");
		return new ResponseEntity<List<CategoryDTO>>(categoryService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/category", method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody CategoryDTO categoryDTO)
	{       
        logger.info("Creating category"); 	
		try
		{
			categoryService.save(categoryDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
    }
	
	
	
	@RequestMapping(value = "/category/{id}", method = RequestMethod.PUT)
	public ResponseEntity<CategoryDTO> update(@PathVariable Short id, @RequestBody CategoryDTO categoryDTO) 
	{
		logger.info("Updating category with id: " + id);		
		try
		{
			CategoryDTO ret = categoryService.update(id, categoryDTO);
			if(ret != null)
			{
				logger.info("Done");
				return new ResponseEntity<CategoryDTO>(ret, HttpStatus.CREATED);			
			}
			else
			{
				logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<CategoryDTO>(HttpStatus.NOT_FOUND);				
			}		
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while updating the actor");
			return new ResponseEntity<CategoryDTO>(HttpStatus.BAD_REQUEST);					
		}		
	}
	
	
	@RequestMapping(value = "/category/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Short> delete(@PathVariable Short id)
	{       
        logger.info("Deleting entity with id: " + id);	      
        try
		{
        	Short ret = categoryService.delete(id);
        	
        	if(ret != null)
        	{
        		logger.info("Done");
        		return new ResponseEntity<Short>(id, HttpStatus.NO_CONTENT);       		
        	}
        	else
        	{
        		logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<Short>(HttpStatus.NOT_FOUND);       		
        	}      	
		}
        catch(Exception e)
		{
			logger.error("An error ocurred while deleting the entity");
			return new ResponseEntity<Short>(HttpStatus.BAD_REQUEST);					
		}	     
    }


}
