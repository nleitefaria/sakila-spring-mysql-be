package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.ActorDTO;
import com.mycompany.myapp.service.ActorService;

@RestController
public class ActorRWS 
{
	@Autowired
	ActorService actorService;
	
	private static final Logger logger = LoggerFactory.getLogger(ActorRWS.class);
	
	@RequestMapping(value = "/actor/{id}", method = RequestMethod.GET)
	public ResponseEntity<ActorDTO> findOne(@PathVariable Short id) {
		logger.info("Listing actor with id: " + id);
		return new ResponseEntity<ActorDTO>(actorService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/actors", method = RequestMethod.GET)
	public ResponseEntity<List<ActorDTO>> findAll() {
		logger.info("Listing all actors");
		return new ResponseEntity<List<ActorDTO>>(actorService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/actor", method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody ActorDTO actorDTO)
	{       
        logger.info("Creating actor"); 	
		try
		{
			actorService.save(actorDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
    }
	
	@RequestMapping(value = "/actor/{id}", method = RequestMethod.PUT)
	public ResponseEntity<ActorDTO> update(@PathVariable Short id, @RequestBody ActorDTO actorDTO) 
	{
		logger.info("Updating actor with id: " + id);		
		try
		{
			ActorDTO ret = actorService.update(id, actorDTO);
			if(ret != null)
			{
				logger.info("Done");
				return new ResponseEntity<ActorDTO>(ret, HttpStatus.CREATED);			
			}
			else
			{
				logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<ActorDTO>(HttpStatus.NOT_FOUND);				
			}		
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while updating the actor");
			return new ResponseEntity<ActorDTO>(HttpStatus.BAD_REQUEST);					
		}		
	}
	
	@RequestMapping(value = "/actor/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Short> delete(@PathVariable Short id)
	{       
        logger.info("Deleting entity with id: " + id);	      
        try
		{
        	Short ret = actorService.delete(id);
        	
        	if(ret != null)
        	{
        		logger.info("Done");
        		return new ResponseEntity<Short>(id, HttpStatus.NO_CONTENT);       		
        	}
        	else
        	{
        		logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<Short>(HttpStatus.NOT_FOUND);       		
        	}      	
		}
        catch(Exception e)
		{
			logger.error("An error ocurred while deleting the entity");
			return new ResponseEntity<Short>(HttpStatus.BAD_REQUEST);					
		}	     
    }

}
