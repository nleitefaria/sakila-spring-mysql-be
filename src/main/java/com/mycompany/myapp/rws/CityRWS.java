package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.CityDTO;
import com.mycompany.myapp.service.CityService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CityRWS 
{	
	@Autowired
	CityService cityService;
	
	private static final Logger logger = LoggerFactory.getLogger(CityRWS.class);
	
	@RequestMapping(value = "/city/{id}", method = RequestMethod.GET)
	public ResponseEntity<CityDTO> findOne(@PathVariable Short id) {
		logger.info("Listing city with id: " + id);
		return new ResponseEntity<CityDTO>(cityService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/cities", method = RequestMethod.GET)
	public ResponseEntity<List<CityDTO>> findAll()
	{
		logger.info("Listing all cities");
		return new ResponseEntity<List<CityDTO>>(cityService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/city", method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody CityDTO cityDTO)
	{       
        logger.info("Creating city"); 	
		try
		{
			cityService.save(cityDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
    }
	
	@RequestMapping(value = "/city/{id}", method = RequestMethod.PUT)
	public ResponseEntity<CityDTO> update(@PathVariable Short id, @RequestBody CityDTO cityDTO) 
	{
		logger.info("Updating city with id: " + id);		
		try
		{
			CityDTO ret = cityService.update(id, cityDTO);
			if(ret != null)
			{
				logger.info("Done");
				return new ResponseEntity<CityDTO>(ret, HttpStatus.CREATED);			
			}
			else
			{
				logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<CityDTO>(HttpStatus.NOT_FOUND);				
			}		
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while updating the actor");
			return new ResponseEntity<CityDTO>(HttpStatus.BAD_REQUEST);					
		}		
	}
	
	@RequestMapping(value = "/city/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Short> delete(@PathVariable Short id)
	{       
        logger.info("Deleting entity with id: " + id);	      
        try
		{
        	Short ret = cityService.delete(id);
        	
        	if(ret != null)
        	{
        		logger.info("Done");
        		return new ResponseEntity<Short>(id, HttpStatus.NO_CONTENT);       		
        	}
        	else
        	{
        		logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<Short>(HttpStatus.NOT_FOUND);       		
        	}      	
		}
        catch(Exception e)
		{
			logger.error("An error ocurred while deleting the entity");
			return new ResponseEntity<Short>(HttpStatus.BAD_REQUEST);					
		}	     
    }



}
