package com.mycompany.myapp.domain;

import java.util.Date;

public class CityDTO
{
	private Short cityId;
	private CountryDTO country;
	private String city;
	private Date lastUpdate;
	
	public CityDTO() 
	{
	}
	
	public CityDTO(CountryDTO country, String city, Date lastUpdate)
	{
		this.country = country;
		this.city = city;
		this.lastUpdate = lastUpdate;
	}

	public CityDTO(Short cityId, CountryDTO country, String city, Date lastUpdate)
	{
		this.cityId = cityId;
		this.country = country;
		this.city = city;
		this.lastUpdate = lastUpdate;
	}

	public Short getCityId() {
		return cityId;
	}

	public void setCityId(Short cityId) {
		this.cityId = cityId;
	}

	public CountryDTO getCountry() {
		return country;
	}

	public void setCountry(CountryDTO country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
}
