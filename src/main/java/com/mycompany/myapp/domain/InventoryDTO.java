package com.mycompany.myapp.domain;

import java.util.Date;

public class InventoryDTO 
{	
	private Integer inventoryId;
	private FilmDTO film;
	private StoreDTO store;
	private Date lastUpdate;
	
	public InventoryDTO()
	{		
	}

	public InventoryDTO(Integer inventoryId, FilmDTO film, StoreDTO store, Date lastUpdate) 
	{
		this.inventoryId = inventoryId;
		this.film = film;
		this.store = store;
		this.lastUpdate = lastUpdate;
	}

	public Integer getInventoryId() {
		return inventoryId;
	}

	public void setInventoryId(Integer inventoryId) {
		this.inventoryId = inventoryId;
	}

	public FilmDTO getFilm() {
		return film;
	}

	public void setFilm(FilmDTO film) {
		this.film = film;
	}

	public StoreDTO getStore() {
		return store;
	}

	public void setStore(StoreDTO store) {
		this.store = store;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
}
