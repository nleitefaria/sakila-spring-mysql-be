package com.mycompany.myapp.domain;

public class FilmTextDTO {
	
	private short filmId;
	private String title;
	private String description;

	public FilmTextDTO() {
	}

	public FilmTextDTO(short filmId, String title) {
		this.filmId = filmId;
		this.title = title;
	}

	public FilmTextDTO(short filmId, String title, String description) {
		this.filmId = filmId;
		this.title = title;
		this.description = description;
	}

	public short getFilmId() {
		return filmId;
	}

	public void setFilmId(short filmId) {
		this.filmId = filmId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
