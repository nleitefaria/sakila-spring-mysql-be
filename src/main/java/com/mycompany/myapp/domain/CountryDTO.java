package com.mycompany.myapp.domain;

import java.util.Date;

public class CountryDTO {
	
	private Short countryId;
	private String country;
	private Date lastUpdate;
	
	public CountryDTO() 
	{	
	}

	public CountryDTO(String country, Date lastUpdate)
	{
		this.country = country;
		this.lastUpdate = lastUpdate;
	}
	
	public CountryDTO(Short countryId, String country, Date lastUpdate)
	{
		this.countryId = countryId;
		this.country = country;
		this.lastUpdate = lastUpdate;
	}

	public Short getCountryId() {
		return countryId;
	}

	public void setCountryId(Short countryId) {
		this.countryId = countryId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
}
