package com.mycompany.myapp.domain;

import java.util.Date;

public class RentalDTO 
{
	private Integer rentalId;
	private CustomerDTO customer;
	private InventoryDTO inventory;
	private StaffDTO staff;
	private Date rentalDate;
	private Date returnDate;
	private Date lastUpdate;
	
	public RentalDTO() {
		
	}

	public RentalDTO(Integer rentalId, CustomerDTO customer, InventoryDTO inventory, StaffDTO staff, Date rentalDate, Date returnDate, Date lastUpdate)
	{	
		this.rentalId = rentalId;
		this.customer = customer;
		this.inventory = inventory;
		this.staff = staff;
		this.rentalDate = rentalDate;
		this.returnDate = returnDate;
		this.lastUpdate = lastUpdate;
	}

	public Integer getRentalId() {
		return rentalId;
	}

	public void setRentalId(Integer rentalId) {
		this.rentalId = rentalId;
	}

	public CustomerDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}

	public InventoryDTO getInventory() {
		return inventory;
	}

	public void setInventory(InventoryDTO inventory) {
		this.inventory = inventory;
	}

	public StaffDTO getStaff() {
		return staff;
	}

	public void setStaff(StaffDTO staff) {
		this.staff = staff;
	}

	public Date getRentalDate() {
		return rentalDate;
	}

	public void setRentalDate(Date rentalDate) {
		this.rentalDate = rentalDate;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	
	
	
	
	

}
