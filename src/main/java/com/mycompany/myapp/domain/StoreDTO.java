package com.mycompany.myapp.domain;

import java.util.Date;

public class StoreDTO 
{
	private Short storeId;
	private AddressDTO address;
	private StaffDTO staff;
	private Date lastUpdate;

	public StoreDTO()
	{		
	}

	public StoreDTO(Short storeId, AddressDTO address, StaffDTO staff ,Date lastUpdate) 
	{	
		this.storeId = storeId;
		this.address = address;
		this.lastUpdate = lastUpdate;
	}

	public Short getStoreId() {
		return storeId;
	}

	public void setStoreId(Short storeId) {
		this.storeId = storeId;
	}

	public AddressDTO getAddress() {
		return address;
	}

	public void setAddress(AddressDTO address) {
		this.address = address;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public StaffDTO getStaff() {
		return staff;
	}

	public void setStaff(StaffDTO staff) {
		this.staff = staff;
	}

}
