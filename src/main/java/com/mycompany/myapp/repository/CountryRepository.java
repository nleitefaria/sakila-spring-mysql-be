package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mycompany.myapp.entity.Country;

public interface CountryRepository extends JpaRepository<Country, Short>, JpaSpecificationExecutor<Country> {

}
