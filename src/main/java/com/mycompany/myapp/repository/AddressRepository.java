package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.mycompany.myapp.entity.Address;

public interface AddressRepository extends JpaRepository<Address, Short>, JpaSpecificationExecutor<Address> {
	@Query("SELECT a FROM  Address a LEFT JOIN FETCH a.city")
	List<Address> findAllCustomers();
}
