package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mycompany.myapp.entity.Actor;

public interface ActorRepository extends JpaRepository<Actor, Short>, JpaSpecificationExecutor<Actor> {

}
