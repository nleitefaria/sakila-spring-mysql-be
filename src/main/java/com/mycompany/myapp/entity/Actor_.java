package com.mycompany.myapp.entity;

import java.util.Date;

import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Actor.class)
public abstract class Actor_ {

	public static volatile SingularAttribute<Actor, String> firstName;
	public static volatile SingularAttribute<Actor, String> lastName;
	public static volatile SingularAttribute<Actor, Short> actorId;
	public static volatile SingularAttribute<Actor, Date> lastUpdate;
	public static volatile SetAttribute<Actor, FilmActor> filmActors;

}
