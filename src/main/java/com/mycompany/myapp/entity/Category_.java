package com.mycompany.myapp.entity;

import java.util.Date;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Category.class)
public abstract class Category_ {

	public static volatile SingularAttribute<Category, Date> lastUpdate;
	public static volatile SetAttribute<Category, FilmCategory> filmCategories;
	public static volatile SingularAttribute<Category, String> name;
	public static volatile SingularAttribute<Category, Short> categoryId;

}
