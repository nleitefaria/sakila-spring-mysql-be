package com.mycompany.myapp.entity;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(FilmCategory.class)
public abstract class FilmCategory_ {

	public static volatile SingularAttribute<FilmCategory, Date> lastUpdate;
	public static volatile SingularAttribute<FilmCategory, FilmCategoryId> id;
	public static volatile SingularAttribute<FilmCategory, Film> film;
	public static volatile SingularAttribute<FilmCategory, Category> category;

}
