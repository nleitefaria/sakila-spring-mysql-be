package com.mycompany.myapp.entity;

import java.util.Date;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(City.class)
public abstract class City_ {

	public static volatile SingularAttribute<City, Country> country;
	public static volatile SetAttribute<City, Address> addresses;
	public static volatile SingularAttribute<City, String> city;
	public static volatile SingularAttribute<City, Date> lastUpdate;
	public static volatile SingularAttribute<City, Short> cityId;

}
