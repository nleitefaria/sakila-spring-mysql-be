package com.mycompany.myapp.entity;

import java.util.Date;

import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Country.class)
public abstract class Country_ {

	public static volatile SingularAttribute<Country, String> country;
	public static volatile SetAttribute<Country, City> cities;
	public static volatile SingularAttribute<Country, Date> lastUpdate;
	public static volatile SingularAttribute<Country, Short> countryId;

}
