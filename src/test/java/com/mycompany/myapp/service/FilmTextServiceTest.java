package com.mycompany.myapp.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.SakilaSpringMysqlBeApplicationTests;
import com.mycompany.myapp.domain.FilmTextDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SakilaSpringMysqlBeApplicationTests.class)
@ActiveProfiles("test")
public class FilmTextServiceTest {
	
	@Autowired
	FilmTextService filmTextService;

	@Test
	public void saveFilmTextTest()
	{
		FilmTextDTO a = new FilmTextDTO((short) 1, "The Space Invaders");
		filmTextService.save(a);
		assertEquals(1, filmTextService.count());
		filmTextService.deleteAll();
	}

	@Test
	public void findAllFilmTextTest() 
	{
		FilmTextDTO a = new FilmTextDTO((short) 1, "The Space Invaders");
		filmTextService.save(a);
		assertEquals(1, filmTextService.findAll().size());
		assertEquals("The Space Invaders", filmTextService.findAll().get(0).getTitle());
		filmTextService.deleteAll();
	}
	
	

}
