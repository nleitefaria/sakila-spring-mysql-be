package com.mycompany.myapp.service;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.SakilaSpringMysqlBeApplicationTests;
import com.mycompany.myapp.domain.ActorDTO;
import com.mycompany.myapp.entity.Actor;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SakilaSpringMysqlBeApplicationTests.class)
@ActiveProfiles("test")
public class ActorServiceTest 
{	
	@Autowired
    ActorService actorService;
	
	@Test
    public void findAllActorsTest()
    {    	
    	ActorDTO a = new ActorDTO("Nuno", "Faria", new Date());  	
    	actorService.save(a);       
        assertEquals(1, actorService.findAll().size());
        assertEquals("Nuno", actorService.findAll().get(0).getFirstName());
        assertEquals("Faria", actorService.findAll().get(0).getLastName());
        actorService.deleteAll();
    }

    @Test
    public void saveActorTest()
    {    	
    	ActorDTO a = new ActorDTO("Nuno", "Faria", new Date());  	
    	actorService.save(a);
        assertEquals(1, actorService.count());
        actorService.deleteAll();
    }

}
