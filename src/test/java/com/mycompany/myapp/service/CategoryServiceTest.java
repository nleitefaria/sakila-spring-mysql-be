package com.mycompany.myapp.service;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.SakilaSpringMysqlBeApplicationTests;
import com.mycompany.myapp.domain.CategoryDTO;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SakilaSpringMysqlBeApplicationTests.class)
@ActiveProfiles("test")
public class CategoryServiceTest 
{
	@Autowired
	CategoryService categoryService;

	@Test
	public void findAllCategoriesTest()
	{
		CategoryDTO categoryDTO = new CategoryDTO("cat1", new Date());
		categoryService.save(categoryDTO);
		assertEquals(1, categoryService.findAll().size());
		assertEquals("cat1", categoryService.findAll().get(0).getName());
		categoryService.deleteAll();
	}
}
