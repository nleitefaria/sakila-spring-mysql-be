package com.mycompany.myapp.service;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.SakilaSpringMysqlBeApplicationTests;
import com.mycompany.myapp.domain.CountryDTO;
import com.mycompany.myapp.entity.Country;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SakilaSpringMysqlBeApplicationTests.class)
@ActiveProfiles("test")
public class CountryServiceTest 
{
	@Autowired
    CountryService countryService;
	
	@Test
    public void findOneCountryTest()
    {    	
		CountryDTO c = new CountryDTO("Portugal", new Date());
		countryService.save(c);  
		
		CountryDTO co = null;
		for(CountryDTO country : countryService.findAll())
		{
			co = countryService.findOne(country.getCountryId());
			assertEquals(country.getCountryId(), co.getCountryId());
			assertEquals(country.getCountry(), co.getCountry());			
		}
        countryService.deleteAll();
    }
	
	@Test
    public void findAllCountriesTest()
    {    	
		CountryDTO c = new CountryDTO("Portugal", new Date());
		countryService.save(c);       
        assertEquals(1, countryService.findAll().size());
        assertEquals("Portugal", countryService.findAll().get(0).getCountry());
        countryService.deleteAll();
    }

}
