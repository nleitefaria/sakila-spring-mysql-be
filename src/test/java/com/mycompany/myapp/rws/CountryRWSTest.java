package com.mycompany.myapp.rws;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mycompany.myapp.SakilaSpringMysqlBeApplicationTests;
import com.mycompany.myapp.domain.CountryDTO;
import com.mycompany.myapp.entity.Country;
import com.mycompany.myapp.service.CityService;
import com.mycompany.myapp.service.CountryService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SakilaSpringMysqlBeApplicationTests.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class CountryRWSTest {
	
	@Autowired
    private MockMvc mockMvc;
	
	@Autowired
    CountryService countryService;
	
	@Autowired
    CityService cityService;
	
	Date date;
	
	@Before
    public void init() throws Exception {
	
		date = new Date();
		cityService.deleteAll();
		countryService.deleteAll();
		CountryDTO countryDTO = new CountryDTO("Portugal", date);
    	countryService.save(countryDTO);
   
    }

    @Test
    public void getAllCountriesTest() throws Exception 
    {        		
        this.mockMvc.perform(get("/countries")).andExpect(status().isOk())
        									.andExpect(content().contentType("application/json;charset=UTF-8"))
        									.andExpect(jsonPath("$", Matchers.hasSize(1)));          
    }
      
    @Test
    public void getCountryTest() throws Exception
    {   	
    	int id = 0;
    	
    	for(CountryDTO c : countryService.findAll())
    	{   		
    		id = c.getCountryId();
    	}
    	
        this.mockMvc.perform(get("/country/" + id)).andExpect(status().isOk())
        									.andExpect(content().contentType("application/json;charset=UTF-8"))
        									.andExpect(content().json("{countryId=" + id + ", country=Portugal, lastUpdate=" + date.getTime() + "}"));      
    }
}
