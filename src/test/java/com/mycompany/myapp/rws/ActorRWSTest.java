package com.mycompany.myapp.rws;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mycompany.myapp.SakilaSpringMysqlBeApplicationTests;
import com.mycompany.myapp.domain.ActorDTO;
import com.mycompany.myapp.service.ActorService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SakilaSpringMysqlBeApplicationTests.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class ActorRWSTest 
{
	@Autowired
    private MockMvc mockMvc;
	
	@Autowired
    ActorService actorService;
	
	Date date;
	
	@Before
    public void init() throws Exception {
	
		date = new Date();
		actorService.deleteAll();
    	ActorDTO a = new ActorDTO("Nuno", "Faria", date);  	
    	actorService.save(a);     
   
    }

    @Test
    public void getAllActorsTest() throws Exception
    {  	    	
        this.mockMvc.perform(get("/actors")).andExpect(status().isOk())
        									.andExpect(content().contentType("application/json;charset=UTF-8"))
        									.andExpect(jsonPath("$", Matchers.hasSize(1)));            
    }
    
    @Test
    public void getActorTest() throws Exception
    {  	
        this.mockMvc.perform(get("/actor/1")).andExpect(status().isOk())
        									.andExpect(content().contentType("application/json;charset=UTF-8"))
        									.andExpect(content().json("{actorId=1, firstName=Nuno, lastName=Faria, lastUpdate=" + date.getTime() + "}"));          
    }
}
