package com.mycompany.myapp.repository;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.SakilaSpringMysqlBeApplicationTests;
import com.mycompany.myapp.entity.Country;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SakilaSpringMysqlBeApplicationTests.class)
@ActiveProfiles("test")
public class CountryRepositoryTest {
	
	@Autowired
	CountryRepository countryRepository;
	
	@Test
    public void findOneCountryTest()
    {    	
		Country c1 = new Country("Portugal", new Date());
		countryRepository.save(c1);      
		
		Country c2 = new Country("Spain", new Date());
		countryRepository.save(c2);      
        
		Country co = null;
		for(Country country : countryRepository.findAll())
		{
			co = countryRepository.findOne(country.getCountryId());
			assertEquals(country.getCountryId(), co.getCountryId());
			assertEquals(country.getCountry(), co.getCountry());			
		}
		
        countryRepository.deleteAll();
    }
	
	@Test
    public void findAllCountriesTest()
    {    	
		Country c = new Country("Portugal", new Date());
		countryRepository.save(c);       
        assertEquals(1, countryRepository.findAll().size());
        assertEquals("Portugal", countryRepository.findAll().get(0).getCountry());
        countryRepository.deleteAll();
    }
	
	@Test
    public void saveCountryTest()
    {    	
		Country c = new Country("Portugal", new Date());
		countryRepository.save(c);       
        assertEquals(1, countryRepository.count());
        countryRepository.deleteAll();
    }

}
