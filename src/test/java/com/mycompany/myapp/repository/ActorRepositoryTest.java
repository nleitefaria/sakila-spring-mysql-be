package com.mycompany.myapp.repository;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.SakilaSpringMysqlBeApplicationTests;
import com.mycompany.myapp.entity.Actor;
import com.mycompany.myapp.repository.ActorRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SakilaSpringMysqlBeApplicationTests.class)
@ActiveProfiles("test")
public class ActorRepositoryTest {
	
	@Autowired
    ActorRepository actorRepository;
	
	@Test
	public void findOneActorTest() 
	{
		Actor a = new Actor("Nuno", "Faria", new Date());  	
    	actorRepository.save(a);       
		Short actorId = a.getActorId();
		
		assertEquals(actorId, actorRepository.findOne(actorId).getActorId());
		assertEquals("Nuno", actorRepository.findOne(actorId ).getFirstName());
		assertEquals("Faria", actorRepository.findOne(actorId ).getLastName());
		actorRepository.deleteAll();
	}
	
	@Test
    public void findAllActorsTest()
    {    	
    	Actor a = new Actor("Nuno", "Faria", new Date());  	
    	actorRepository.save(a);       
        assertEquals(1, actorRepository.findAll().size());
        assertEquals("Nuno", actorRepository.findAll().get(0).getFirstName());
        assertEquals("Faria", actorRepository.findAll().get(0).getLastName());
        actorRepository.deleteAll();
    }

    @Test
    public void saveActorTest()
    {    	
    	Actor a = new Actor("Nuno", "Faria", new Date());  	
    	actorRepository.save(a);
        assertEquals(1, actorRepository.count());
        actorRepository.deleteAll();
    }

}
