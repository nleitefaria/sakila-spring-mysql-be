package com.mycompany.myapp.repository;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.SakilaSpringMysqlBeApplicationTests;
import com.mycompany.myapp.entity.FilmText;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SakilaSpringMysqlBeApplicationTests.class)
@ActiveProfiles("test")
public class FilmTextRepositoryTest 
{
	@Autowired
	FilmTextRepository filmTextRepository;

	@Test
	public void saveFilmTextTest()
	{
		FilmText a = new FilmText((short) 1, "The Space Invaders");
		filmTextRepository.save(a);
		assertEquals(1, filmTextRepository.count());
		filmTextRepository.deleteAll();
	}

	@Test
	public void findAllFilmTextTest() 
	{
		FilmText a = new FilmText((short) 1, "The Space Invaders");
		filmTextRepository.save(a);
		assertEquals(1, filmTextRepository.findAll().size());
		assertEquals("The Space Invaders", filmTextRepository.findAll().get(0).getTitle());
		filmTextRepository.deleteAll();
	}
}
