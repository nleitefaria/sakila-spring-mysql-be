package com.mycompany.myapp.repository;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.SakilaSpringMysqlBeApplicationTests;
import com.mycompany.myapp.entity.Address;
import com.mycompany.myapp.entity.City;
import com.mycompany.myapp.entity.Country;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SakilaSpringMysqlBeApplicationTests.class)
@ActiveProfiles("test")
public class AddressRepositoryTest 
{
	@Autowired
	AddressRepository addressRepository;

	@Autowired
	CityRepository cityRepository;

	@Autowired
	CountryRepository countryRepository;

	@Test
	public void findOneActorTest() {
		Country country = new Country("Portugal", new Date());
		countryRepository.save(country);
		City city = new City(country, "Oporto", new Date());
		cityRepository.save(city);

		Address address = new Address(city, "Rua XPTO", "d1", "123456789", new Date());
		addressRepository.save(address);
		Short addressId = address.getAddressId();

		assertEquals(addressId, addressRepository.findOne(addressId).getAddressId());
		assertEquals("Rua XPTO", addressRepository.findOne(addressId).getAddress());
		assertEquals("d1", addressRepository.findOne(addressId).getDistrict());
		assertEquals("123456789", addressRepository.findOne(addressId).getPhone());

		addressRepository.deleteAll();
		cityRepository.deleteAll();
		countryRepository.deleteAll();
	}

	@Test
	public void findAllTest() {
		Country country = new Country("Portugal", new Date());
		countryRepository.save(country);
		City city = new City(country, "Oporto", new Date());
		cityRepository.save(city);

		Address address1 = new Address(city, "Rua YPTO", "d1", "123456789", new Date());
		addressRepository.save(address1);
		Address address2 = new Address(city, "Rua YPTO", "d1", "123456789", new Date());
		addressRepository.save(address2);

		List<Address> addressList = addressRepository.findAll();
		assertEquals(2, addressList.size());

		addressRepository.deleteAll();
		cityRepository.deleteAll();
		countryRepository.deleteAll();
	}
	
	@Test
	public void saveActorTest() 
	{		
		Country country = new Country("Portugal", new Date());
		countryRepository.save(country);
		City city = new City(country, "Oporto", new Date());
		cityRepository.save(city);

		Address address = new Address(city, "Rua XPTO", "d1", "123456789", new Date());
		addressRepository.save(address);
		
		assertEquals(1, addressRepository.findAll().size());
		
		addressRepository.deleteAll();
		cityRepository.deleteAll();
		countryRepository.deleteAll();
	}

}
