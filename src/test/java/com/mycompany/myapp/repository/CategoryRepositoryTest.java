package com.mycompany.myapp.repository;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.SakilaSpringMysqlBeApplicationTests;
import com.mycompany.myapp.entity.Category;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SakilaSpringMysqlBeApplicationTests.class)
@ActiveProfiles("test")
public class CategoryRepositoryTest 
{
	@Autowired
	CategoryRepository categoryRepository;
	
	@Test
	public void findOneCategoryTest() 
	{
		Category category = new Category("category1", new Date());	
		categoryRepository.save(category);
		Short categoryId = category.getCategoryId();	
		assertEquals(categoryId,categoryRepository.findOne(categoryId).getCategoryId());		
		categoryRepository.deleteAll();
	}
	
	@Test
    public void findAllActorsTest()
    {    		
		Category category = new Category("category1", new Date());	
		categoryRepository.save(category);
		assertEquals(1, categoryRepository.findAll().size());
		assertEquals("category1", categoryRepository.findAll().get(0).getName());
		categoryRepository.deleteAll();	
    }
	
	@Test
    public void saveActorTest()
    {    	
		Category category = new Category("category1", new Date());	
		categoryRepository.save(category);
        assertEquals(1, categoryRepository.count());
        categoryRepository.deleteAll();
    }

}
