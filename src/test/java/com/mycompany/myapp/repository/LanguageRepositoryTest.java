package com.mycompany.myapp.repository;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.SakilaSpringMysqlBeApplicationTests;
import com.mycompany.myapp.entity.FilmText;
import com.mycompany.myapp.entity.Language;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SakilaSpringMysqlBeApplicationTests.class)
@ActiveProfiles("test")
public class LanguageRepositoryTest 
{
	@Autowired
	LanguageRepository languageRepository;
	
	@Test
	public void findOneLanguageTest() 
	{
		Language language = new Language("Portuguese", new Date());
		languageRepository.save(language);
		Short languageId = language.getLanguageId();
		
		assertEquals(languageId, languageRepository.findOne(languageId).getLanguageId());
		assertEquals("Portuguese", languageRepository.findOne(languageId).getName());
		languageRepository.deleteAll();
	}
	
	@Test
	public void findAllLanguageTest() 
	{
		Language language = new Language("Portuguese", new Date());
		languageRepository.save(language);
		assertEquals(1, languageRepository.findAll().size());
		assertEquals("Portuguese",languageRepository.findAll().get(0).getName());
		languageRepository.deleteAll();
	}

	@Test
	public void saveLanguageTest()
	{
		Language language = new Language("Portuguese", new Date());
		languageRepository.save(language);
		assertEquals(1, languageRepository.count());
		languageRepository.deleteAll();
	}
	
	
	
	
	


}
