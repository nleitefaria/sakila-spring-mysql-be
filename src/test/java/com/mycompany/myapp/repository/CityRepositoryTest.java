package com.mycompany.myapp.repository;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.SakilaSpringMysqlBeApplicationTests;
import com.mycompany.myapp.entity.City;
import com.mycompany.myapp.entity.Country;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SakilaSpringMysqlBeApplicationTests.class)
@ActiveProfiles("test")
public class CityRepositoryTest {
	
	@Autowired
    CityRepository cityRepository;
	
	@Autowired
    CountryRepository countryRepository;

	@Test
    public void findAllCitiesTest()
    {    	
    	Country country = new Country("Portugal", new Date());
    	countryRepository.save(country);
    	City a = new City(country, "Oporto", new Date());
    	cityRepository.save(a);
        assertEquals(1, cityRepository.findAll().size());
        cityRepository.deleteAll();
        countryRepository.deleteAll();
    }
	
    @Test
    public void saveCityTest()
    {    	
    	Country country = new Country("Portugal", new Date());
    	countryRepository.save(country);
    	City a = new City(country, "Oporto", new Date());
    	cityRepository.save(a);
        assertEquals(1, cityRepository.count());
        cityRepository.deleteAll();
        countryRepository.deleteAll();
    }

}
